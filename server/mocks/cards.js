'use strict';

const { default: carddetails } = require('../../app/components/carddetails');

module.exports = function(app) {
  const express = require('express');
  let cardsRouter = express.Router();
  
  var cards=[
    {id: 1,
      title:'Task 1',
      description:'Login at 9AM',
      comments: 
        [{ comment: '', date: '' }] 
    },
    {id: 2,
      title:'Task 2',
      description:'Attend meeting at 10AM',
      comments: 
        [{ comment: '', date: '' }]
    }
  
    ];

  cardsRouter.get('/', function(req, res) {
    var data=[];
cards.forEach(function(item) {
  data.push({
    type:'cards',
     id: item.id.toString(),
     attributes:{
       title: item.title,
       description:item.description,
       comments:item.comments
     }
  })
})
    res.set('Content-Type','application/vnd.api+json');
    res.send({    
  // 'cards': cards
      data: data
    });
  });

  cardsRouter.post('/', function(req, res) {
    var newcard=req.body.data.attributes;
    var newId= cards.length+1;
    // newcard.id= newId;
    cards.push(
{
title: newcard.title,
description: newcard.description,
comments:newcard.comments,
id: newId
} );
res.set('Content-Type','application/vnd.api+json');
    res.send({
      data:
      {
       type:'cards',
       id:newId,
       attributes:newcard
      }
    });
  });

  cardsRouter.get('/:id', function(req, res) {
    res.send({
      'cards': {
        id: req.params.id
      }
    });
  });

  cardsRouter.patch('/:id', function(req, res) {
var cardAttrs=req.body.data.attributes;
var cardId=req.params.id;
cards.forEach(function(item) {
  if(item.id===parseInt(cardId))
  {
item.title=cardAttrs.title;
item.description=cardAttrs.description;
item.comments=cardAttrs.comments;

  }
});
    res.send({
      data:{
        type:'cards',
        id:cardId,
        attributes:cardAttrs

      }


    });
  });

cardsRouter.delete('/:id', function(req, res) {
  var cardId=req.params.id;
for(var i=0;i<cards.length;i++)
{
  if(parseInt(cardId)===cards[i].id)
  {
  cards.splice(i,1);
  break;
  }
}
res.status(204).end();
  });

  // The POST and PUT call will not contain a request body
  // because the body-parser is not included by default.
  // To use req.body, run:

  //    npm install --save-dev body-parser

  // After installing, you need to `use` the body-parser for
  // this mock uncommenting the following line:
  //
  //app.use('/api/cards', require('body-parser').json());
  app.use('/api/cards', cardsRouter);
};
