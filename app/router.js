import EmberRouter from '@ember/routing/router';
import config from 'taskmanager/config/environment';

export default class Router extends EmberRouter {
  location = config.locationType;
  rootURL = config.rootURL;
}

Router.map(function() {
  this.route('cards',{path: '/cards/:card_id'});
  this.route('newcard');
});
