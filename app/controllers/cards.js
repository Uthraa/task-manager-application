export default Ember.Controller.extend(
    {
actions: {

    editCard: function(cards){
       var _this=this;
       cards.save().then(function(card) {
        _this.transitionToRoute("index");
    })
},
deleteCard: function(cards){
    // alert(cards.title);
   var _this=this;
   cards.destroyRecord().then(function(card) {
    _this.transitionToRoute("index");
})

},
addComment: function(cards){
    var _this=this;
    cards.save().then(function(card) {
     _this.transitionToRoute("cards");
 })
}

    }
    }
);
