import Ember from 'ember';

export default Ember.Controller.extend(
    {
actions: {

    saveDetails: function(){
        Date.prototype.toShortFormat = function() {

            let monthNames =["Jan","Feb","Mar","Apr",
                              "May","Jun","Jul","Aug",
                              "Sep", "Oct","Nov","Dec"];
            
            let day = this.getDate();
            
            let monthIndex = this.getMonth();
            let monthName = monthNames[monthIndex];
            
            let year = this.getFullYear();
            
            return (day+' '+monthName+' '+year);  
        }
        var title=this.get('cardtitle');
        var description=this.get('carddescription');
        var comment=this.get('comment');
        if(!title)title='Card title';
        var card;
        if(!comment)
        {
            var card={title: title,
                description:description,
                comments:[{}]};
        }
        else{
            var date=new Date().toShortFormat();
            var card={title: title,
                description:description,
                comments:[{comment:comment,date:date}]
        };
        }
        var _this=this;
        this.store.createRecord('card',card).save().then(function(card) {
            _this.transitionToRoute("index");
        })
    }
}

    }

);

