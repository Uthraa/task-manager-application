import Ember from 'ember';

export default Ember.Component.extend(
    
    
    {
    actions: {
      editCard: function(cards) {
        this.sendAction('submit',cards);
      },
      deleteCard: function(cards) {
      this.sendAction('delete',cards);
      },
      addComment: function(cards) {
        var comment=this.get('commentbox');
        Date.prototype.toShortFormat = function() {

            let monthNames =["Jan","Feb","Mar","Apr",
                              "May","Jun","Jul","Aug",
                              "Sep", "Oct","Nov","Dec"];
            
            let day = this.getDate();
            
            let monthIndex = this.getMonth();
            let monthName = monthNames[monthIndex];
            
            let year = this.getFullYear();
            
            return (day+' '+monthName+' '+year);  
        }
        
        var date=new Date().toShortFormat();
        var cardId=0;
        var newcomment={comment,date};
       while(cards.comments[cardId]!=null)
        cardId++;
        cards.comments.addObject(newcomment);
        this.setProperties({commentbox:''});
        this.sendAction('comment',cards);
        
        }
    }
  });