import Ember from 'ember';

export default Ember.Route.extend({
model: function(param) {
  
  return this.store.findRecord('card', param.card_id);
    
},
    
    setupController: function(controller, model) {
      controller.set('cards', model) ;
    },



});